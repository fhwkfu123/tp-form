import { linkObject } from './layoutObject';

export default [
  linkObject({name: 'Link', label: '链接', setting: {
    disabled: false,
    type: 'primary',
    underline: true,
    href: '',
    icon: '',
    target: "_blank",
    text: '这里是链接文本'
  }})
]